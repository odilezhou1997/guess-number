package game;

import java.util.Arrays;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.stream.Stream;

public class GuessResult {
    private Map<String, String> guessRecord;
    private static final int CHANCE_LIMIT = 6;
    
    public GuessResult(Map<String, String> guessRecord) {
        this.guessRecord = guessRecord;
    }
    
    
    public String getResult() {
        Set<String> keySet = guessRecord.keySet();
        String[] keyArray = keySet.toArray(new String[0]);
        Arrays.sort(keyArray);

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < keyArray.length; i++) {
            if ((String.valueOf(guessRecord.get(keyArray[i]))).trim().length() > 0) {
                sb.append(keyArray[i]).append(" ").append(String.valueOf(guessRecord.get(keyArray[i])).trim());
            }
            if(i != keyArray.length-1){
                sb.append("\n");
            }
        }
        return sb.toString();
    }
    
    public GameResult getGameResult() {
        if (guessRecord.containsValue("4A0B")) {
            return GameResult.WIN;
        } else if (guessRecord.size() != 6) {
            return GameResult.NORMAL;
        }
        return GameResult.LOST;
    }
}
