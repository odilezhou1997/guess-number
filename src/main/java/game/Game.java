package game;

import answer.Answer;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Game {
    
    private Answer answer;
    private Map<String, String> guessResult;
    
    public Game() {
        Path filePath = Paths.get("answer.txt");
        this.answer = new Answer(filePath);
        this.guessResult = new LinkedHashMap<>();
    }
    
    public String guess(List<Integer> numbers) {
        String result = "";
        if (numbers.equals(answer.getAnswer())) {
            result = "4A0B";
        } else {
            int countA = getASize(numbers);
            int countB = getBSize(numbers);
            result = countA + "A" + countB + "B";
        }
        String key = numbers.stream().map(String::valueOf).collect(Collectors.joining(""));
        this.guessResult.put(key, result);
        return result;
    }
    
    public boolean isOver() {
        return guessResult.containsValue("4A0B") || guessResult.size() == 6;
    }
    
    public String getResult() {
        StringBuilder result = new StringBuilder();
        for (Map.Entry<String, String> item : guessResult.entrySet()) {
            result.append(item.getKey()).append(" ").append(item.getValue()).append("\n");
        }
        if (guessResult.containsValue("4A0B")) {
            result.append("Congratulations, you win!");
        } else {
            result.append("Unfortunately, you have no chance, the answer is ").append(answer.toString()).append("!");
        }
        return result.toString();
    }
    
    private int getBSize(List<Integer> numbers) {
        return (int) numbers.stream()
                .filter(num ->
                        answer.getAnswer().contains(num) && answer.getAnswer().indexOf(num) != numbers.indexOf(num))
                .count();
    }
    
    private int getASize(List<Integer> numbers) {
        return (int) numbers.stream().filter(num -> answer.getAnswer().indexOf(num) == numbers.indexOf(num)).count();
    }
}
