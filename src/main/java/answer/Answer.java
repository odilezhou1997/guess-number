package answer;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.HashSet;

public class Answer {
    
    private static final int ANSWER_LENGTH = 4;
    private static final int ANSWER_NUMBER_LIMIT = 10;
    private List<Integer> answer = null;
    
    public Answer(Path filePath) {
        try {
            List<Integer> numbers = getAnswerFromFile(filePath);
            validAnswer(numbers);
            this.setAnswer(numbers);
        } catch (Exception e) {
            this.setAnswer(generateRandomAnswer());
        }
    }
    
    public List<Integer> getAnswer() {
        return this.answer;
    }
    
    public void setAnswer(List<Integer> answer){
        this.answer = answer;
    }
    
    @Override
    public String toString() {
        return answer.stream().map(String::valueOf).collect(Collectors.joining(""));
    }
    
    private List<Integer> getAnswerFromFile(Path filePath) throws IOException {
        ClassLoader classLoader = getClass().getClassLoader();
        InputStream is = classLoader.getResourceAsStream(String.valueOf(filePath));

        InputStreamReader isr = new InputStreamReader(is, StandardCharsets.UTF_8);
        BufferedReader br = new BufferedReader(isr);

        return Arrays.stream(br.readLine().split("")).map(Integer::valueOf).collect(Collectors.toList());
    }
    
    private List<Integer> generateRandomAnswer() {
        Random random = new Random();
        //unrepeated number
        HashSet<Integer> randomList = new HashSet<>();
        do {
            int temp = random.nextInt(9) + 1;
            randomList.add(temp);
        } while (randomList.size() != ANSWER_LENGTH);
        return new ArrayList<>(randomList);
    }
    
    static void validAnswer(List<Integer> answer) throws InvalidAnswerException {
        List<Integer> newList = answer.stream().distinct().collect(Collectors.toList());
        if (answer.size() != ANSWER_LENGTH || newList.size() != ANSWER_LENGTH) {
            throw new InvalidAnswerException("input\tWrong input, input again");
        }
    }
}
